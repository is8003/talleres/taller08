#include <stdio.h>

unsigned int subcadena(char letras[], unsigned int n);

int main(void){
	puts("Ejemplo 1:");
	printf("Longitud: %u\n", subcadena("tirabuzon", 9));

	puts("");
	puts("Ejemplo 2:");
	printf("Longitud: %u\n", subcadena("arepa", 5));
}


unsigned int subcadena(char letras[], unsigned int n){
	unsigned int pos = 0;					// Declaro variable pos para almacenar inicio de subcadena e inicializo valor a cero.
	unsigned int tam = 0;					// Declaro variable tam para almacenar tamaño de subcadena e inicializo valor a cero.

	for (size_t i = 0; i < n - 1; i++){			// Para i desde 0; hasta i menor que n menos 1; incremento i en 1:
		size_t j = i;					// Declaro j y asigno valor de i.

		do{						// Hacer:
			j++;					// Incremento j en 1.

		}while (j < n && letras[j - 1] < letras[j]);	// Mientras j sea menor a n y caracter letra[j - 1] sea menor que caracter letra[j].

		if (j - i > tam){				// Si final de subcadena j menos inicio de subcadena i es mayor a tam:

			tam = j - i;				// Asigno a tamaño de subcadena más larga tam la diferencia entre j e i.
			pos = i;				// Asigno a inicio de subcadena más larga pos el valor de i.
			i = j - 1;				// Continúo búsqueda de subcadena a partir de posición j.
		}
	}

	for (size_t i = pos; i < pos + tam; i++){		// Para i desde pos; hasta i menor que pos mas tam; incremento i en 1:
		printf("%c", letras[i]);			// Imprimo caracter de subcadena ascendente más larga.
	}

	puts("");

	return tam;						// Retorno tamaño tam de subcadena ascendente más larga.
}
