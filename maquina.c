#include <stdio.h>

unsigned int maquina(unsigned int final);

int main(void){
	unsigned int e1Num = 11;

	puts("Ejemplo 1:");
	printf("Entrada: %u\n", e1Num);
	printf("Longitud: %u\n", maquina(e1Num));

	unsigned int e2Num = 351;

	puts("Ejemplo 2:");
	printf("Entrada: %u\n", e2Num);
	printf("Longitud: %u\n", maquina(e2Num));
}

unsigned int maquina(unsigned int final){
	unsigned int longitud = 0;			// Creo variable longitud e inicializo a 0.
	char sequencia[65536];				// Creo arreglo sequencia de 65536 caracteres.

	while (final > 0){				// Mientras final sea mayor a cero:
		if (final % 2 == 0){			// Si módulo 2 de final es cero:
			sequencia[longitud] = 'A';	// Asigno 'A' a elemento en posición longitud de arreglo sequencia.
			final /= 2;			// Divido final en 2.
		}
		else{					// De lo contrario:
			sequencia[longitud] = 'B';	// Asigno 'B' a elemento en posición longitud de arreglo sequencia.
			final--;			// Resto 1 a final.
		}

		longitud++;				// Icremento longitud en 1.
	}

	printf("%s","Sequencia: ");
	for (int i = longitud - 1; i >= 0; i--){	// Para i desde longitud menos 1; hasta i mayor igual a 0; decremento i en 1:
		printf("%c", sequencia[i]);		// Imprino caracter en posición i de arreglo sequencia.
	}

	puts("");					// Imprimo cambio de línea.

	return longitud;				// Retorno longitud.
}
