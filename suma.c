#include <stdio.h>

void suma(unsigned numero[], unsigned n);

int main(void){
	puts("Ejemplo 1:");
	unsigned e1N = 4;
	unsigned e1Numero[4] = { 5, 15, 6, 4};

	suma(e1Numero, e1N);

	puts("Ejemplo 2:");
	unsigned e2N = 6;
	unsigned e2Numero[6] = { 1, 2, 3, 4, 5, 16};

	suma(e2Numero, e2N);
}

void suma(unsigned numero[], unsigned n){
	unsigned sumatoria = numero[0];
	unsigned iMax = 0;

	for (unsigned i = 1; i < n; i++){
		sumatoria += numero[i];
		if (numero[i] > numero[iMax]) iMax = i;
	}

	if (numero[iMax] == sumatoria - numero[iMax]){
		printf("Sí: %d\n", numero[iMax]);
	}
	else{
		puts("No.");
	}
}
