# Suma

Escriba la función `suma` que reciba como parámetro un entero positivo `n <=
10` que indica la cantidad de números en el vector, y un vector `numero` con
números enteros. La función debe determinar si alguno de estos números es igual
a la suma de los restantes n-1. En caso de una respuesta afirmativa debe decir
cuál es.

## Refinamiento 1

### Función: `suma`

#### Entradas:

- `numero`: arreglo con números enteros.
- `n`: tamaño de arreglo `numero`.

#### Salidas:

- Ninguna (impresión en pantalla).

#### Pseudo-código:

1. Recorro arreglo `numero` sumando cada elemento a sumatoria y buscando el
   elemento mayor.
1. Si elemento mayor es igual a sumatoria menos elemento mayor:
   1. Imprimo "Sí" y valor de elemento mayor.
1. De lo contrario (no encontré elemento):
   1. Imprimo "No".

## Refinamiento 2

### Función: `suma`

#### Entradas:

- `numero`: arreglo con números enteros.
- `n`: tamaño de arreglo `numero`.

#### Salidas:

- Ninguna (impresión en pantalla).

#### Pseudo-código:

1. Declaro variable entera `sumatoria` y asigno valor de elemento en posición
   `0` de arreglo `sumatoria`.
1. Declaro variable entera `iMax` (posición de elemento mayor) y asigno `0`.
1. Para `i` igual a `1`; hasta `i` menor a `n`; incremento `i` en `1`:
   1. Incremento elemento en posición `i` de arreglo `numero` a `sumatoria`.
   1. Si elemento en posición `i` es mayor que en posición `iMax`:
      1. Asigno `i` a `iMax`.
1. Si elemento en posición `iMax` es igual a `sumatoria` menos elemento en
   posición `iMax`:
   1. Imprimo "Sí" y valor de elemento en posición `iMax`.
1. De lo contrario (no encontré elemento):
   1. Imprimo "No".

# Máquina

Una máquina tiene dos botones llamados A y B. En un tablero digital se muestra
un número. Al presionar A el número se duplica y al presionar B se le suma uno.
El número inicial de la máquina siempre es cero. Elabore una función `maquina`
que reciba como entrada el número final que aparece en el tablero y encuentre e
imprima una secuencia de As y Bs con longitud mínima con la cual se pueda
obtener ese número. La función debe devolver la longitud mínima.

## Refinamiento 1

### Función `maquina`

Imprime en pantalla la secuencias de As (duplica número) y Bs (suma uno) con
longitud mínima y retorna este valor.

#### Entradas:

- `final`: número que aparece en el tablero.

#### Salidas:

- `longitud`: cantidad mínima de As y Bs para obtener número final.

#### Seudo-códgio:

1. Creo variable `longitud` e inicializo a `0`.
1. Creo arreglo `sequencia` de `65536` caracteres.
1. Mientras `final` sea mayor a cero:
   1. Si módulo `2` de `final` es cero:
      1. Asigno 'A' a elemento en posición `longitud` de arreglo `sequencia`.
      1. Divido `final` en `2`.
   1. De lo contrario:
      1. Asigno 'B' a elemento en posición `longitud` de arreglo `sequencia`.
      1. Resto `1` a `final`.
   1. Icremento longitud en `1`.
1. Para `i` desde `longitud` menos `1`; hasta `i` mayor igual a `0`; decremento
   `i` en `1`:
   1. Imprino caracter en posición `i` de arreglo `sequencia`.
1. Imprimo cambio de línea.
1. Retorno `longitud`.

# Subcadena más larga

Escriba la función `subcadena` que reciba como entrada un vector de caracteres
llamado `letras` y un valor `n` que indica el número de caracteres en el
vector, la función debe determinar el tamaño de la subcadena más grande que
tenga todos sus caracteres en orden ascendente e imprimir en la función la
subcadena.

## Refinamiento 1

### Función: `subcadena`

Determina el tamaño de la subcadena con sequencia de caracteres en orden
ascendente más grande e imprime en pantalla subcadena.

#### Entradas
- `letras`: arreglo de caracteres.
- `n`: tamaño de arreglo `letras`.

#### Salidas

- `longitud`: tamaño de subcadena.

#### Pseudo-código

1. Declaro variable `pos` para almacenar inicio de subcadena e inicializo valor
   a cero.
1. Declaro variable `tam` para almacenar tamaño de subcadena e inicializo valor
   a cero.
1. Para `i` desde `0`; hasta `i` menor que `n` menos `1`; incremento `i` en
   `1`:
   1. Declaro `j` y asigno valor de `i`.
   1. Hacer:
      1. Incremento `j` en `1`.
   1. Mientras `j` sea menor a `n` y caracter `letra[j - 1]` sea menor que
      caracter `letra[j]`.
   1. Si final de subcadena `j` menos inicio de subcadena `i` es mayor a `tam`:
      1. Asigno a tamaño de subcadena más larga `tam` la diferencia entre `j` e
	 `i`.
      1. Asigno a inicio de subcadena más larga `pos` el valor de `i`.
      1. Continúo búsqueda de subcadena a partir de posición `j`.
1. Para `i` desde `pos`; hasta `i` menor que `pos` mas `tam`; incremento `i` en `1`:
   1. Imprimo caracter de subcadena ascendente más larga.
1. Retorno tamaño `tam` de subcadena ascendente más larga.
